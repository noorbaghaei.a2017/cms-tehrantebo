<?php

namespace Modules\Lottery\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Lottery\Entities\Lottery;
use Spatie\Permission\Models\Permission;

class LotteryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Lottery::class)->delete();

        Permission::create(['name'=>'lottery-list','model'=>Lottery::class,'created_at'=>now()]);
        Permission::create(['name'=>'lottery-create','model'=>Lottery::class,'created_at'=>now()]);
        Permission::create(['name'=>'lottery-edit','model'=>Lottery::class,'created_at'=>now()]);
        Permission::create(['name'=>'lottery-delete','model'=>Lottery::class,'created_at'=>now()]);

    }
}
