@include('core::layout.modules.index',[

    'title'=>__('core::categories.index'),
    'items'=>$items,
    'parent'=>'article',
    'model'=>'article',
    'directory'=>'articles',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'create_route'=>['name'=>'article.category.create'],
    'edit_route'=>['name'=>'article.category.edit','name_param'=>'category'],
    'pagination'=>false,
    'parent_route'=>true,
    'datatable'=>[
         __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
             __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
   __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])



