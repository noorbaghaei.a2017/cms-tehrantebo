@include('core::layout.modules.index',[

    'title'=>__('product::attributes.index'),
    'items'=>$items,
    'parent'=>'product',
    'model'=>'product',
    'singular_value'=>'attribute',
    'popular_value'=>'attributes',
    'directory'=>'attributes',
    'collect'=>__('product::attributes.collect'),
    'singular'=>__('product::attributes.singular'),
    'create_route'=>['name'=>'product.create.attribute'],
    'edit_route'=>['name'=>'product.edit.attribute','name_param'=>'attribute'],
     'pagination'=>true,
      'setting_route'=>true,
          'option_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
      __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
