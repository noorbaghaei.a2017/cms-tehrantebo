<?php

namespace Modules\Service\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Question\Entities\Question;
use Modules\Question\Transformers\QuestionCollection;
use Modules\Service\Entities\Service;

class ServiceCollection extends ResourceCollection
{
    public $collects = Service::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new ServiceResource($item);
                }
            ),
            'filed' => [
                'thumbnail','title','slug','status','update_date','create_date'
            ],
            'public_route'=>[

                [
                    'name'=>'services.create',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ],
                [
                    'name'=>'service.categories',
                    'param'=>[null],
                    'icon'=>config('cms.icon.categories'),
                    'title'=>__('cms.categories'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-warning pull-right',
                    'method'=>'GET',
                ]
            ],
            'private_route'=>
                [
                    [
                        'name'=>'services.edit',
                        'param'=>[
                            'service'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'services.destroy',
                        'param'=>[
                            'service'=>'token'
                        ],
                        'icon'=>config('cms.icon.delete'),
                        'title'=>__('cms.delete'),
                        'class'=>'btn btn-danger btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'destroy',
                        'method'=>'DELETE',
                    ],
                    [
                        'name'=>'services.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ]
                ],
            'search_route'=>[

                'name'=>'search.service',
                'filter'=>[
                    'title','slug','order'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('service::services.collect'),
            'title'=>__('service::services.index'),
        ];
    }
}
