<?php


namespace Modules\Event\Entities\Repository;


use Modules\Event\Entities\Event;

class EventRepository implements EventRepositoryInterface
{

    public function getAll()
    {
        return Event::latest()->get();
    }
}
