<?php

namespace Modules\Information\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\User;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Models\Role;
use Spatie\Tags\HasTags;

class Information extends Model implements HasMedia
{
    use HasTags,Sluggable,HasMediaTrait,TimeAttribute;

    protected $table="informations";

    protected $fillable = ['title','text','excerpt','token','slug','user','refer_link','short_link','category'];

    public $filed = ['title','slug','view','like','questions','update_date','create_date'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function user_info()
    {
        return $this->belongsTo(User::class,'user','id');
    }


    public  function getViewAttribute(){

        return $this->analyzer->view;
    }

    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }
    public  function getQuestionAttribute(){

        return $this->questions()->count();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(400)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(60)
            ->performOnCollections(config('cms.collection-image'));
    }

    /**
     * @inheritDoc
     */public function sluggable(): array
    {
        return [
            'slug'=>[
                'source'=>'title'
            ]
        ];
    }

}
