<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید تصویر جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید تصویر خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست تصاویر",
    "singular"=>"تصویر",
    "collect"=>"تصاویر",
    "permission"=>[
        "questions-full-access"=>"دسترسی کامل به تصاویر",
        "questions-list"=>"لیست تصاویر",
        "questions-delete"=>"حذف تصویر",
        "questions-create"=>"ایجاد تصویر",
        "questions-edit"=>"ویرایش تصویر",
    ]
];
