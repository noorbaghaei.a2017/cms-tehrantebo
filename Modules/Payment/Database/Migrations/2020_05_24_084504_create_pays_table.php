<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user');
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->string('amount');
            $table->string('description');
            $table->string('authority')->unique();
            $table->string('card_number');
            $table->string('trace_number');
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('gateway')->default(0);
            $table->tinyInteger('type')->default(0);
            $table->string('token')->unique();
            $table->morphs('paymentable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pays');
    }
}
