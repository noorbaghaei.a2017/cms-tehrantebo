

    <li>
        <a href="{{route('payments.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('payment.icons.payment')}}"></i>
                              </span>
            <span class="nav-text"> {{__('payment::payments.collect')}}</span>
        </a>
    </li>

