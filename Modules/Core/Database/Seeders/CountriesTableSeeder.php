<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->delete();

        $countries=[

            [
                'code' => 'IR',
                'name' => 'Iran',
                'phonecode' => '98',
                'created_at' =>now(),
            ],
            [
                'code' => 'DE',
                'name' => 'Germany',
                'phonecode' => '49',
                'created_at' =>now(),
            ]

        ];

        DB::table('countries')->insert($countries);
    }
}
