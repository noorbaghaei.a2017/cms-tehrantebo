@extends('core::layout.panel')
@section('pageTitle', ' ویرایش')
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <h2> {{__('cms.title')}}   {{$item->name}}</h2>
                            <small>
                                با استفاده از فرم زیر میتوانید مقاله مورد نظر را مشاهده کنید.
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{route('roles.update', ['role' => $item->token])}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            {{method_field('PATCH')}}
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="name" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" value="{{$item->name}}" name="name" class="form-control" id="name" required>
                                </div>
                                <div class="col-sm-7">
                                    <span class="text-danger">*</span>
                                    @include('core::layout.roles.list-permission',['data'=>$rolePermissions])


                                </div>

                            </div>

                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <input type="submit"  class="btn btn-success btn-sm text-sm" value="{{__('cms.update')}} ">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    name: {
                        required: true
                    },
                    permission: {
                        required: true
                    },


                },
                messages: {
                    name:"عنوان الزامی است",
                    permission: "دسترسی  لزامی است",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
