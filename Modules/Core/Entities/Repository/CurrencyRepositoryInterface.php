<?php


namespace Modules\Core\Entities\Repository;


interface CurrencyRepositoryInterface
{
    public function getAll();

}
