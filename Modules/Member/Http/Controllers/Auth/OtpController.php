<?php

namespace Modules\Member\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Entities\Client;
use Modules\Client\Http\Requests\OtpSendRequest;
use Modules\Sms\Http\Controllers\Gateway\FarazSms;

class OtpController extends Controller
{

    public  function otpClient(Request $request)
    {

        return view('template.auth.verifies.otp-form', compact('client'));

    }

    public  function submitOtp(OtpSendRequest $request)
    {
        try {

            $code=strval(rand(1000,9999));
            if(filter_var($request->identify,FILTER_VALIDATE_EMAIL)){
                $client=Client::whereEmail($request->identify)->firstOrFail();

                $client->update([
                    'code'=>$code,
                    'code_expire'=>now()->addMinutes(10),
                ]);

                $token=$client->token;

                $data=[
                    'code'=>$client->code
                ];
                sendOtpEmail($data);

                return view('template.auth.verifies.otp',compact('token'));

            }else{
                $client=Client::whereMobile($request->identify)->firstOrFail();

                $client->update([
                    'code'=>$code,
                    'code_expire'=>now()->addMinutes(10),
                ]);

                $token=$client->token;

                $kavengar=new FarazSms();

                $message=$client->code;

                $sms=[
                    'sender'=>['+9850009227180825'],
                    'mobile'=>[$client->mobile],
                    'message'=>$message
                ];
                $kavengar->sendMessage($sms);

                return view('template.auth.verifies.otp',compact('token'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }


    }
}
