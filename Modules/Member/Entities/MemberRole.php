<?php

namespace Modules\Member\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;

class MemberRole extends Model
{
    use TimeAttribute;

    protected $table="member_roles";
    protected $fillable = ['title','token'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public  function getTransTitleAttribute(){

        return __('cms.'.$this->title);
    }
}
