<?php
return [
    "text-create"=>"you can create your skill",
    "text-edit"=>"you can edit your skill",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"skills list",
    "error"=>"error",
    "singular"=>"skill",
    "collect"=>"skills",
    "permission"=>[
        "skill-full-access"=>"skill full access",
        "skill-list"=>"skills list",
        "skill-delete"=>"skill delete",
        "skill-create"=>"skill create",
        "skill-edit"=>"edit skill",
    ]


];
