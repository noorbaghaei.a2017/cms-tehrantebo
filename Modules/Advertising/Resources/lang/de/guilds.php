<?php
return [
    "text-create"=>"you can create your guild",
    "text-edit"=>"you can edit your guild",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"guilds list",
    "error"=>"error",
    "singular"=>"guild",
    "collect"=>"guilds",
    "permission"=>[
        "guild-full-access"=>"guild full access",
        "guild-list"=>"guilds list",
        "guild-delete"=>"guild delete",
        "guild-create"=>"guild create",
        "guild-edit"=>"edit guild",
    ]


];
