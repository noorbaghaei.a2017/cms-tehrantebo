<?php

namespace Modules\Portfolio\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Portfolio\Entities\Portfolio;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Portfolio::class)->delete();

        Permission::create(['name'=>'portfolio-list','model'=>Portfolio::class,'created_at'=>now()]);
        Permission::create(['name'=>'portfolio-create','model'=>Portfolio::class,'created_at'=>now()]);
        Permission::create(['name'=>'portfolio-edit','model'=>Portfolio::class,'created_at'=>now()]);
        Permission::create(['name'=>'portfolio-delete','model'=>Portfolio::class,'created_at'=>now()]);
    }
}
