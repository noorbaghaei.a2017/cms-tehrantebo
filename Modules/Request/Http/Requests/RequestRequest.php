<?php

namespace Modules\Request\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required',
            'name'=>'required',
            'phone'=>'required',
            'Vorne'=>'mimes:jpeg|file|max:3000',
            'Hinten'=>'mimes:jpeg|file|max:3000',
            'Oben'=>'mimes:jpeg|file|max:3000',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
