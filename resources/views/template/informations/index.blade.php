@extends('template.app')

@section('content')


    <div class="full_page_photo no_photo">
        <div class="container">
            <div class="hgroup">
                <div class="hgroup_title animated bounceInUp skincolored">
                    <div class="container">
                        <h1>اخبار</h1>
                    </div>
                </div>
                <div class="hgroup_subtitle animated bounceInUp ">

                </div>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="container triangles-of-section">
            <div class="triangle-up-left"></div>
            <div class="square-left"></div>
            <div class="triangle-up-right"></div>
            <div class="square-right"></div>
        </div>
        <section class="with_right_sidebar">
            <div class="container">
                <div class="row">
                    <div id="leftcol" class="col-sm-8 col-md-8">
                        @foreach($items as $item)
                        <article class="post">
                            <div class="post_header">
                                <h3 class="post_title"><a href="{{route('informations.single',['information'=>$item->slug])}}">{{$item->title}}</a></h3>
                                <div class="post_sub"><i class="fa-time"></i> {{Morilog\Jalali\Jalalian::forge($item->created_at)->format('%A, %d %B %y')}}</div>
                            </div>
                            <div class="post_content">
                                <figure class="text-right"><a href="{{route('informations.single',['information'=>$item->slug])}}" >
                                        @if(!$item->Hasmedia('images'))
                                        <img alt="0" src="{{asset('img/no-img.gif')}}" width="600" height="400">
                                        @else
                                            <img alt="0" src="{{$item->getFirstMediaUrl('images')}}" width="600" height="400">
                                        @endif
                                    </a>
                                </figure>
                                <p>{{$item->excerpt}}</p>
                                <a href="{{route('informations.single',['information'=>$item->slug])}}" class="btn btn-primary">بیشتر بخوانید</a> </div>
                        </article>
                        @endforeach

                    </div>
                    <div id="sidebar" class="col-sm-4 col-md-4">
                        <aside class="widget ads clearfix">
                            <h4>تبلیغات</h4>
                            <a href="#"><img src="http://placehold.it/110" alt=""></a> <a href="#"><img src="http://placehold.it/110" alt=""></a> <a href="#"><img src="http://placehold.it/110" alt=""></a>
                        </aside>

                    </div>
                </div>
            </div>
        </section>



@endsection



