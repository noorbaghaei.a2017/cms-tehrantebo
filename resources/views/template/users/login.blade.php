﻿@extends('template.sections.user.app')
@section('content')
    <form action="{{ route('client.login.submit') }}" method="POST" class="login100-form validate-form p-l-55 p-r-55 p-t-178" style="direction: rtl">

        @csrf

					<span class="login100-form-title">
						پنل کاربران
					</span>
        @error('email')
        <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
        <div class="wrap-input100 validate-input m-b-16" data-validate="لطفا نام کاربری را وارد کنید">
            <input class="input100" type="text" name="email" placeholder="نام کاربری">
            <span class="focus-input100"></span>
        </div>
        @error('password')
        <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
        <div class="wrap-input100 validate-input" data-validate = "لطفا کلمه عبور را وارد کنید">
            <input class="input100" type="password" name="password" placeholder="کلمه عبور">
            <span class="focus-input100"></span>
        </div>


        <div class="container-login100-form-btn">
            <button class="login100-form-btn">
                ورود
            </button>
        </div>

    </form>
@endsection






