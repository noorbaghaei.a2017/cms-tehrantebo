﻿@extends('template.app')

@section('content')

@include('template.sections.sliders')

<div class="main">
        <div class="container triangles-of-section">
            <div class="triangle-up-left"></div>
            <div class="square-left"></div>
            <div class="triangle-up-right"></div>
            <div class="square-right"></div>
        </div>
@include('template.sections.properties')
        <section class="call_to_action dark_section">
            <div class="container">
                <h3>رویداد ها</h3>
{{--                <a class="btn btn-primary" href="{{route('about-us')}}">اطلاعات بیشتر</a> </div>--}}

                <div class="portfolio_strict row">
                    @foreach($events as $event)
                        <div class="col-sm-4 col-md-4">
                            <div class="portfolio_item wow fadeInUp"> <a href="{{route('events.single',['event'=>$event->slug])}}">
                                    @if(!$event->Hasmedia('images'))
                                        <div class="full_page_photo" style="background-image: url({{asset('template/images/portfolio/b3.jpg')}});"> </div>
                                    @else
                                                <figure style="background-image:url({{$event->getFirstMediaUrl('images')}})">
                                    @endif

                                        <figcaption>
                                            <div class="portfolio_description">
                                                <h3>{{$event->title}}</h3>
                                                <span class="cross"></span>
                                                <p>{{$event->excerpt}}</p>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </a> </div>
                        </div>
                    @endforeach

                </div>
{{--                <div class="centered_button"><a class="btn btn-primary" href="{{route('portfolios')}}">رویداد های دیگر</a></div>--}}
        </section>
        <section class="portfolio_teasers_wrapper">
            <div class="container triangles-of-section">
                <div class="triangle-up-left"></div>
                <div class="square-left"></div>
                <div class="triangle-up-right"></div>
                <div class="square-right"></div>
            </div>
           @include('template.sections.portfolios')
        </section>

@include('template.sections.members')

@endsection



