@extends('template.app')

@section('content')

    <!-- Start Class Details -->
    <section class="page-class-details bg--white section-padding--lg">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 text-right" style="direction: rtl">
                    <div class="class__quality__desc">

                        <div class="class__thumb">
                            <div class="courses__type d-flex justify-content-between">
                                <ul class="d-flex">
                                    <li>دسته بندی : <span>{{$item->nameCategory}}</span></li>
                                    <span>/</span>
                                    <li>تاریخ شروع : <span>{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('%A, %d %B %y')}}</span></li>
                                </ul>
                                <ul class="rating d-flex">
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                    <li><span class="ti-star"></span></li>
                                </ul>
                            </div>
                            <div class="courses__images">
                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('template/images/class/big-img/1.jpg')}}" alt="class images">
                                @else

                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="class images" >
                                @endif

                            </div>
                            <ul class="courses__meta d-flex">
                                <li class="prize">{{$item->PriceFormat}}</li>
                                <li><i class="fa fa-eye"></i>{{$item->view}}</li>
                                <li><i class="fa fa-heart"></i>{{$item->like}}</li>
                            </ul>
                        </div>
                        <div class="class-details-inner">
                            <div class="courses__inner">
                                <h2>{{$item->title}}</h2>
                                <ul>
                                    <li>مدت زمان  : <span>{{$item->total_hour}}</span></li>
                                    <li>تاریخ پایان  :  <i class="fa fa-calendar"></i><span>{{Morilog\Jalali\Jalalian::forge($item->end_at)->format('%A, %d %B %y')}}</span></li>
                                </ul>
                               <p>
                                   {!! $item->text !!}
                               </p>
                            </div>

                            <div class="class__nav nav justify-content-start" role="tablist">
                                <a class="nav-item nav-link active" data-toggle="tab" href="#nav-view" role="tab">توضیحات</a>
                                <a class="nav-item nav-link " data-toggle="tab" href="#nav-teacher" role="tab">استاد</a>

                            </div>

                            <div class="class__tab__content--inner">
                                <div class="class__view single-class-content tab-pane fade show active" id="nav-view" role="tablist">
                                    <h2>توضیحات</h2>

                                    <p>
                                        {!! $item->excerpt !!}
                                    </p>

                                    <div class="share__us">
                                        <span>اشتراک گذاری</span>

                                        <ul class="dacre__social__link d-flex justify-content-center">
                                            <li class="facebook"><a target="_blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                            <li class="twitter"><a target="_blank" href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                            <li class="vimeo"><a target="_blank" href="https://www.vimeo.com/"><i class="fa fa-vimeo"></i></a></li>
                                            <li class="pinterest"><a target="_blank" href="https://pinterest.com"><i class="fa fa-pinterest"></i></a></li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="teacher__view single-class-content tab-pane fade" id="nav-teacher" role="tablist">

                                    <!-- Start Dingle Teacher -->
                                    <div class="teacher">
                                        <img src="{{asset('template/images/amin-nourbaghaei.jpeg')}}" alt="teacher">
                                        <h2>امین نوربقایی</h2>
                                        <span>برنامه نویس</span>


                                        <ul class="class__service d-flex justify-content-between flex-wrap flex-lg-nowrap">
                                            <li>
                                                <span class="count wow">90</span>
                                                <span class="title">زبان انگلیسی</span>
                                            </li>
                                            <li>
                                                <span class="count wow">80</span>
                                                <span class="title">کلاس آنلاین</span>
                                            </li>
                                            <li>
                                                <span class="count wow">70</span>
                                                <span class="title">تدریس خصوصی</span></li>
                                            <li>
                                                <span class="count wow">40</span>
                                                <span class="title">تدریس عمومی</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- End Dingle Teacher -->




                                </div>

                            </div>
                        </div>
                        <!-- Start Related Class -->
                        <div class="related__class__wrapper">
                            <h2 class="title">کلاس های مشابه</h2>
                            <div class="row">

                            @foreach($last_courses as $last_course)
                                <!-- Start Single Classs -->
                                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                    <div class="related__classs">
                                        <div class="calss__thumb">
                                            <a href="{{route('courses.single',['course'=>$last_course->slug])}}">
                                                @if(!$last_course->Hasmedia('images'))
                                                    <img src="{{asset('template/images/class/big-img/1.jpg')}}" alt="class images">
                                                @else

                                                    <img src="{{$last_course->getFirstMediaUrl('images')}}" alt="class images" >
                                                @endif

                                            </a>
                                        </div>
                                        <div class="class__details">
                                            <h2><a href="{{route('courses.single',['course'=>$last_course->slug])}}">{{$last_course->title}}</a></h2>
                                        </div>
                                        <div class="class__hover__action">
                                            <div class="classs__hover__inner">
                                                <div class="courses__fee">
                                                    <span>50%</span>
                                                </div>
                                                <h2><a href="{{route('courses.single',['course'=>$last_course->slug])}}">{{$last_course->title}}</a></h2>
                                                <ul>
                                                    <li>تاریخ شروع : {{Morilog\Jalali\Jalalian::forge($item->end_at)->format('%A, %d %B %y')}}</li>
                                                    <li>ظرفیت : {{$last_course->capacity}}</li>
                                                </ul>
                                                <div class="class__btn">
                                                    <a class="dcare__btn" href="{{route('courses.single',['course'=>$last_course->slug])}}">ثبت نام</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Classs -->


                                @endforeach

                            </div>
                        </div>
                        <!-- End Related Class -->

                    </div>
                </div>
                <!-- Start Sidebar -->
                <div class="col-lg-3">
                    <div class="sidebar__widgets sidebar--right">



                        <!-- Single Widget -->
                        <div class="single__widget about">
                            <div class="about__content">
                                <img src="{{asset('template/images/others/1.jpg')}}" alt="about images">
                            </div>
                        </div>
                        <!-- End Widget -->

                        <!-- Single Widget -->
                        <div class="single__widget recent__post text-right">
                            <h4>آخرین کلاس ها</h4>
                            <ul>
                                @foreach($last_courses as $last_course)

                                <li>
                                    <a href="{{route('courses.single',['course'=>$last_course->slug])}}">
                                        @if(!$last_course->Hasmedia('images'))
                                            <img src="{{asset('template/images/courses/1.jpg')}}" alt="post images">
                                        @else

                                            <img src="{{$last_course->getFirstMediaUrl('images')}}" alt="post images" >
                                        @endif
                                    </a>
                                    <div class="post__content">
                                        <h6><a href="{{route('courses.single',['course'=>$last_course->slug])}}">{{$last_course->title}}</a></h6>
                                        <span class="date"><i class="fa fa-calendar"></i>{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('%A, %d %B %y')}}</span>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- End Widget -->

                        <!-- Single Widget -->
                        <div class="single__widget offer">
                            <div class="offer__thumb">
                                <img src="{{asset('template/images/others/2.jpg')}}" alt="offer images">
                            </div>
                        </div>
                        <!-- End Widget -->



                        <!-- Single Widget -->
                        <div class="single__widget Popular__classes text-right">
                            <h4>کلاس های محبوب</h4>
                            <ul>
                                @foreach($popular_courses as $popular_course)
                                    <li><a href="{{route('courses.single',['course'=>$popular_course->slug])}}">{{$popular_course->title}}</a></li>
                                @endforeach

                            </ul>
                        </div>
                        <!-- End Widget -->



                    </div>
                </div>
                <!-- End Sidebar -->
            </div>
        </div>
    </section>
    <!-- End Class Details -->


@endsection



