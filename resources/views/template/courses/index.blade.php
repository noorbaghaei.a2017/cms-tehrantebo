@extends('template.app')

@section('content')

    <!-- Start Courses Area -->
    <section class="dcare__courses__area section-padding--lg bg--white">
        <div class="container">
            <div class="row class__grid__page" style="direction: rtl">
                <!-- Start Single Courses -->
                @foreach($items as $item)
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="courses">
                        <div class="courses__thumb">
                            <a href="{{route('courses.single',['course'=>$item->slug])}}">

                                @if(!$item->Hasmedia('images'))
                                    <img src="{{asset('template/images/courses/1.jpg')}}" alt="courses images">
                                @else

                                    <img src="{{$item->getFirstMediaUrl('images')}}" alt="courses images" >
                                @endif
                            </a>
                        </div>
                        <div class="courses__inner">
                            <ul class="courses__meta d-flex">
                                <li class="prize">{{$item->PriceFormat}}</li>
                                <li class="comment"><i class="fa fa-eye"></i>{{$item->view}}</li>
                                <li class="like"><i class="fa fa-heart"></i>{{$item->like}}</li>
                            </ul>
                            <div class="courses__wrap text-right">
                                <div class="courses__date"><i class="fa fa-calendar"></i>{{Morilog\Jalali\Jalalian::forge($item->start_at)->format('%A, %d %B %y')}}</div>
                                <div class="courses__content">
                                    <h4><a href="{{route('courses.single',['course'=>$item->slug])}}">{{$item->title}}</a></h4>
                                    <span>ظرفیت : {{$item->capacity}} نفر</span>
                                    <p>
                                        {{$item->excerpt}}
                                    </p>
                                    <ul class="rating d-flex">
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                        <li><span class="ti-star"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
                <!-- End Single Courses -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="dcare__pagination mt--80">
                        <ul class="dcare__page__list d-flex justify-content-center">
                            <li><a href="#"><span class="ti-angle-double-left"></span></a></li>
                            <li><a class="page" href="#">بعدی</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                            <li><a href="#">28</a></li>
                            <li><a class="page" href="#">قبلی</a></li>
                            <li><a href="#"><span class="ti-angle-double-right"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection



