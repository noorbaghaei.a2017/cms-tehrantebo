@extends('template.app')

@section('content')
    <div class="full_page_photo no_photo">
        <div class="hgroup">
            <div class="hgroup_title animated bounceInUp skincolored">
                <div class="container">
                    <h1 class="">نمونه کار</h1>
                </div>
            </div>
            <div class="hgroup_subtitle animated bounceInUp ">
                <div class="container">
                    <p>لیست نمونه کارها</p>
                </div>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="container triangles-of-section">
            <div class="triangle-up-left"></div>
            <div class="square-left"></div>
            <div class="triangle-up-right"></div>
            <div class="square-right"></div>
        </div>
        <section class="portfolio_strict">
            <div class="container">
                <ul class="portfolio_filters">
                    <li><a href="#" data-filter="*">همه</a></li>
                    @foreach($all_category_portfolios as $category)
                    <li><a href="#" data-filter=".{{$category->id}}">{{$category->symbol}}</a></li>
                    @endforeach

                </ul>
                <div class="row isotope_portfolio_container">
                    @foreach($items as $item)
                    <div class="{{$item->category}} col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="portfolio_item"> <a href="{{route('portfolios.single',['portfolio'=>$item->slug])}}">
                                @if(!$item->Hasmedia('images'))
                                    <figure style="background-image:url({{asset('img/no-img.gif')}})">
                                @else
                                    <figure style="background-image:url({{$item->getFirstMediaUrl('images')}})">
                                @endif

                                    <figcaption>
                                        <div class="portfolio_description">
                                            <h3>{{$item->title}}</h3>
                                            <span class="cross"></span>
                                            <p>{{$item->excerpt}}</p>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a> </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </section>
@endsection



