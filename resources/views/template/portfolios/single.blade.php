@extends('template.app')

@section('content')
    <div class="main">
        <div class="container triangles-of-section">
            <div class="triangle-up-left"></div>
            <div class="square-left"></div>
            <div class="triangle-up-right"></div>
            <div class="square-right"></div>
        </div>
        <section class="portfolio_slider_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-8">
                        <div class="portfolio_slider_wrapper">
                            <div class="flexslider" id="portfolio_slider">
                                <ul class="slides">
                                    @if(!$item->Hasmedia('images'))
                                        <li class="item" data-thumb="{{asset('img/no-img.gif')}}" style="background-image: url({{asset('img/no-img.gif')}})">
                                            <div class="container"> <a href="{{asset('img/no-img.gif')}}" class="lightbox_portfolio" title="title on a"></a>
                                                <div class="carousel-caption">
                                                    <p class="lead">{{$item->excerpt}}</p>
                                                </div>
                                            </div>
                                        </li>
                                    @else
                                        <li class="item" data-thumb="{{$item->getFirstMediaUrl('images')}}" style="background-image: url({{$item->getFirstMediaUrl('images')}})">
                                            <div class="container"> <a href="{{$item->getFirstMediaUrl('images')}}" class="lightbox_portfolio" title="title on a"></a>
                                                <div class="carousel-caption">
                                                    <p class="lead">{{$item->excerpt}}</p>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                        @if(count($medias) > 0)
                                            @foreach($medias as $media)
                                                <li class="item" data-thumb="{{$media->getUrl()}}" style="background-image: url({{$media->getUrl()}})">
                                                    <div class="container"> <a href="{{$media->getUrl()}}" class="lightbox_portfolio" title="title on a"></a>
                                                        <div class="carousel-caption">
                                                            <p class="lead">{{$item->excerpt}}</p>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        @else

                                        @endif


                                </ul>
                            </div>
{{--                            <div id="carousel" class="flexslider">--}}
{{--                                <ul class="slides">--}}
{{--                                    @if(!$item->Hasmedia('images'))--}}
{{--                                        <li> <img src="{{asset('img/no-img.gif')}}" alt=""> </li>--}}
{{--                                    @else--}}
{{--                                        <li> <img src="{{$item->getFirstMediaUrl('images')}}" alt=""> </li>--}}
{{--                                    @endif--}}
{{--                                        @if(count($medias) > 0)--}}
{{--                                            @foreach($medias as $media)--}}
{{--                                                <li> <img src="{{$media->getUrl()}}" alt=""> </li>--}}

{{--                                            @endforeach--}}
{{--                                        @else--}}


{{--                                        @endif--}}


{{--                                </ul>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="portfolio_details">
                            <h2 class="section_header">درباره این پروژه</h2>

                            {!! $item->text !!}
                            <br>
                            <br>
                            <div>
                                <p><strong>تاریخ:</strong> {{Morilog\Jalali\Jalalian::forge($item->create_at)->format('%A, %d %B %y')}}</p>
{{--                                <p><strong>مشتری:</strong> شرکت ملی نفت</p>--}}
{{--                                <p><strong>دسته بندی:</strong> طراحی</p>--}}
{{--                                <p><strong>مکان:</strong> اروپا</p>--}}

                            </div>
                            <br>
                            <br>
{{--                            <a href="#" class="btn btn-danger center-block ">مشاهده سایت پروژه</a> </div>--}}
                    </div>
                </div>
{{--                <ul class="pager">--}}
{{--                    <li class="next"><a href="#">← قدیمی تر</a></li>--}}
{{--                    <li class="previous disabled"><a href="#">جدیدتر →</a></li>--}}
{{--                </ul>--}}
            </div>
        </section>
{{--        <section class="portfolio_teasers_wrapper">--}}
{{--            <div class="container">--}}
{{--                <h2 class="elegant centered section_header">پروژه های مرتبط<small></small></h2>--}}
{{--                <div class="portfolio_strict row">--}}
{{--                    <div class="col-sm-4 col-md-4">--}}
{{--                        <div class="portfolio_item"> <a href="portfolio_item.html">--}}
{{--                                <figure style="background-image:url(images/portfolio/b2.jpg)">--}}
{{--                                    <figcaption>--}}
{{--                                        <div class="portfolio_description">--}}
{{--                                            <h3>پروژه میهن وبمستر</h3>--}}
{{--                                            <span class="cross"></span>--}}
{{--                                            <p>مردم</p>--}}
{{--                                        </div>--}}
{{--                                    </figcaption>--}}
{{--                                </figure>--}}
{{--                            </a> </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4 col-md-4">--}}
{{--                        <div class="portfolio_item"> <a href="portfolio_item.html">--}}
{{--                                <figure style="background-image:url(images/portfolio/t5.jpg)">--}}
{{--                                    <figcaption>--}}
{{--                                        <div class="portfolio_description">--}}
{{--                                            <h3>پروژه میهن وبمستر</h3>--}}
{{--                                            <span class="cross"></span>--}}
{{--                                            <p>مردم</p>--}}
{{--                                        </div>--}}
{{--                                    </figcaption>--}}
{{--                                </figure>--}}
{{--                            </a> </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4 col-md-4">--}}
{{--                        <div class="portfolio_item"> <a href="portfolio_item.html">--}}
{{--                                <figure style="background-image:url(images/portfolio/p3.jpg)">--}}
{{--                                    <figcaption>--}}
{{--                                        <div class="portfolio_description">--}}
{{--                                            <h3>پروژه میهن وبمستر</h3>--}}
{{--                                            <span class="cross"></span>--}}
{{--                                            <p>مردم</p>--}}
{{--                                        </div>--}}
{{--                                    </figcaption>--}}
{{--                                </figure>--}}
{{--                            </a> </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
@endsection
