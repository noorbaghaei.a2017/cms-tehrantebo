@extends('template.app')

@section('content')
    @if(!$item->Hasmedia('images'))
        <div class="full_page_photo" style="background-image: url({{asset('img/no-img.gif')}});"> </div>
    @else
        <div class="full_page_photo" style="background-image: url({{$item->getFirstMediaUrl('images')}});"> </div>
    @endif

    <div class="main">
        <div class="container triangles-of-section">
            <div class="triangle-up-left"></div>
            <div class="square-left"></div>
            <div class="triangle-up-right"></div>
            <div class="square-right"></div>
        </div>
        <section class="with_right_sidebar">
            <div class="container">
                <h2 class="section_header elegant">{{$item->title}} <small style="margin-top: 10px"><i class="fa-time"></i> {{Morilog\Jalali\Jalalian::forge($item->create_at)->format('%A, %d %B %y')}}  </small></h2>
                <div class="row">
                    <div id="leftcol" class="col-sm-8 col-md-8">
                        <article class="post">
                            <div class="post_content">
                             {!! $item->text !!}
                              </div>
                        </article>

                    </div>
                    <div id="sidebar" class="col-sm-4 col-md-4">

                        <aside class="widget ads clearfix">
                            <h4>تبلیغات</h4>
                            <a href="#"><img src="http://placehold.it/110" alt=""></a> <a href="#"><img src="http://placehold.it/110" alt=""></a> <a href="#"><img src="http://placehold.it/110" alt=""></a> </aside>
                        <aside class="widget">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active"><a data-toggle="tab" href="#recent">جدید ترین ها</a></li>
                                <li class=""><a data-toggle="tab" href="#tags">برچسب ها</a></li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div id="recent" class="tab-pane fade active in">
                                    <ul class="media-list">
                                       @foreach($events as $event)
                                            <li class="media"> <a href="{{route('events.single',['event'=>$event->slug])}}" class="media-photo" style="background-image:url(images/portfolio/t5.jpg)"></a> <a href="#" class="media-date">{{Morilog\Jalali\Jalalian::forge($item->create_at)->format('%A, %d %B %y')}}</a>
                                                <h5 class="media-heading"><a href="{{route('events.single',['event'=>$event->slug])}}">{{$event->title}}</a></h5>
                                                <p>{{$event->excerpt}}</p>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>
                                <div id="tags" class="tab-pane fade">
                                   @foreach($item->tags as $tag)

                                        <a class="label label-primary"> {{$tag->name}}</a>

                                    @endforeach

                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>


@endsection
