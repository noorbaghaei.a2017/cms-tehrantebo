<section class="twitter_feed_wrapper skincolored_section">
    <div class="container">
        <div class="row">
            <div class="twitter_feed_icon wow fadeInDown"><a href="https://twitter.com/PlethoraThemes"><i class="fa fa-twitter"></i></a></div>
            <div id="twitter_flexslider" class="flexslider">
                <ul class="slides">
                    @foreach($properties as $property)
                    <li class="item">
                        <blockquote>
<p>
    {{$property->text}}
</p>
                        </blockquote>
                    </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</section>
<footer>
    <section class="footer_teasers_wrapper dark_section">
        <div class="container triangles-of-section">
            <div class="triangle-up-left"></div>
            <div class="square-left"></div>
            <div class="triangle-up-right"></div>
            <div class="square-right"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="footer_teaser pl_about_us_widget col-sm-4 col-md-4">
                    <h3>در تماس باشید</h3>
                    <p>{{$setting->address}}</p>
                        <p class="info"><a style="color: #fdfdfd" href="mailto:{{$setting->email}}"><i class="fa fa-envelope" style="margin-left: 5px"></i></a> {{$setting->email}}</p>
                    <p class="info"><a style="color: #fdfdfd" href="tel:{{$setting->email}}"><i class="fa fa-phone" style="margin-left: 5px"></i> </a> {{json_decode($setting->phone,true)[0]}}</p>
                    <div class="footer_social">
                        <div class="social_wrapper">
                            <a href="https://telegram.me/{{$setting->info->telegram}}"><i class="fa fa-telegram"></i></a>
                            <a href="https://www.instagram.com/{{$setting->info->instagram}}"><i class="fa fa-instagram"></i></a>
                            <a href="https://wa.me/{{$setting->info->whatsapp}}"><i class="fa fa-whatsapp"></i></a>
                        </div>
                    </div>
                </div>
                <div class="footer_teaser pl_latest_news_widget col-sm-4 col-md-4">
                    <h3>آخرین اخبار</h3>
                    <ul class="media-list">
                        @foreach($new_informations as $information)
                        <li class="media">
                            @if(!$information->Hasmedia('images'))
                                <a  href="{{route('informations.single',['information'=>$information->slug])}}" class="media-photo" style="background-image:url({{asset('img/no-img.gif')}})"></a>
                            @else
                                <a href="{{route('informations.single',['information'=>$information->slug])}}" class="media-photo" style="background-image:url({{$information->getFirstMediaUrl('images')}})"></a>

                            @endif

                            <a style="color: #fdfdfd" href="#" class="media-date">{{Morilog\Jalali\Jalalian::forge($information->create_at)->format(' %B')}}<span>{{Morilog\Jalali\Jalalian::forge($information->create_at)->format( '%d')}}</span></a>
                            <h5 class="media-heading"><a href="{{route('informations.single',['information'=>$information->slug])}}">{{$information->title}}</a></h5>
                            <p>{{$information->excerpt}}</p>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="footer_teaser pl_flickr_widget col-sm-4 col-md-4">
                    <h3>اطلاعات بیشتر</h3>
                    <ul>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="copyright">
        <div class="container">
            <div class="row text-right">
                <div class="col-sm-12 col-md-12">کلیه حقوق کپی رایت برای این شرکت محفوظ می باشد </div>
            </div>
        </div>
    </div>
</footer>
</div>
<script src="{{asset('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{asset('template/twitter-bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<!--Pace Page Loader -->
<script src="{{asset('template/js/pace-0.5.1/pace.min.js')}}"></script>
<!--FlexSlider -->
<script src="{{asset('template/js/woothemes-FlexSlider-06b12f8/jquery.flexslider-min.js')}}"></script>
<!--Isotope Plugin -->
<script src="{{asset('template/js/isotope/jquery.isotope.min.js')}}" type="text/javascript"></script>
<!--To-Top Button Plugin -->
<script type="text/javascript" src="{{asset('template/js/jquery.ui.totop.js')}}"></script>
<!--Easing animations Plugin -->
<script type="text/javascript" src="{{asset('template/js/easing.js')}}"></script>
<!--WOW Reveal on scroll Plugin -->
<script type="text/javascript" src="{{asset('template/js/wow.min.js')}}"></script>
<!--Simple Text Rotator -->
<script type="text/javascript" src="{{asset('template/js/jquery.simple-text-rotator.js')}}"></script>
<!--The Theme Required Js -->
<script type="text/javascript" src="{{asset('template/js/cleanstart_theme.js')}}"></script>
<!--To collapse the menu -->
<script type="text/javascript" src="{{asset('template/js/collapser.js')}}"></script>

</div>
</body>
</html>
