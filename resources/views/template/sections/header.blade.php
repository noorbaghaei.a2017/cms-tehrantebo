<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    {!! SEO::generate() !!}

    @yield('seo')
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="google-site-verification" content="{{env('GOOGLE_VERIFY')}}" />
    <!-- All Animations CSS -->
    <link href="{{asset('template/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('template/css/animate-rtl.css')}}" rel="stylesheet" type="text/css">
    <!-- Image Lightbox CSS-->
    <link rel="stylesheet" href="{{asset('template/css/imagelightbox.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{asset('template/css/imagelightbox-rtl.css')}}" type="text/css" media="screen">
    <!-- Theme styles and Menu styles -->
    <link href="{{asset('template/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('template/css/style-rtl.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('template/css/mainmenu.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('template/css/mainmenu-rtl.css')}}" rel="stylesheet" type="text/css">
{{--    <link href="{{asset('template/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">--}}
    <!-- Call Fontawsome Icon Font from a CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    {{--    <link href="{{asset('template/css/font-awesome.css')}}" rel="stylesheet" type="text/css">--}}
{{--    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">--}}
{{--    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">--}}
<!--Pace Page Loader -->
    <link rel="stylesheet" href="{{asset('template/js/pace-0.5.1/themes/pace-theme-minimal.css')}}" type="text/css" media="screen" />
    <!--FlexSlider -->
    <link rel="stylesheet" href="{{asset('template/js/woothemes-FlexSlider-06b12f8/flexslider.css')}}" type="text/css" media="screen">
    <!--Isotope Plugin -->
    <link rel="stylesheet" href="{{asset('template/js/isotope/css/style.css')}}" type="text/css" media="screen">
    <!--Simple Text Rotator -->
    <link href="{{asset('template/css/simpletextrotator.css')}}" rel="stylesheet" type="text/css">
    <!--Style Switcher, You propably want to remove this!-->
    <link href="{{asset('template/css/_style_switcher.css')}}" rel="stylesheet" type="text/css">
    <!--Modernizer Custom -->
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <script type="text/javascript" src="{{asset('template/js/modernizr.custom.48287.js')}}"></script>
    <!-- Fav and touch icons -->
</head>
<body class="sticky_header">
<div class="overflow_wrapper">
    <header>
        <div class="container">
            <div class="logo">
                <a class="brand" href="{{route('front.website')}}">
                    @if(!$setting->Hasmedia('logo'))
                        <img src="{{asset('img/no-img.gif')}}" alt="optional logo">
                    @else
                        <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="optional logo">
                    @endif

                </a>
            </div>
           @include('template.sections.menu')
            <div class="triangle-up-left"></div>
            <div class="triangle-up-right"></div>
        </div>
    </header>
