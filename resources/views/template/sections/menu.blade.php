<div id="mainmenu" class="menu_container">
    <label class="mobile_collapser">منو</label>
    <!-- Mobile menu title -->
    <ul>
        @foreach($top_menus as $menu)
        <li class="active"><a href="{{$menu->href}}">{{$menu->symbol}}</a></li>
        @endforeach
    </ul>
</div>
