

<section class="features_teasers_wrapper">
    <div class="container">
        <div class="row">
            @foreach($properties as $property)
            <div class="feature_teaser col-sm-4 col-md-4"> <img alt="responsive" src="{{asset('template/images/phone-v2.png')}}">
                <h3>{{$property->title}}</h3>
<p>
    {{$property->text}}
</p>
            </div>

            @endforeach
        </div>
    </div>
</section>
