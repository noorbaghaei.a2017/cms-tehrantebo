<section class="team_members">
    <div class="container triangles-of-section">
        <div class="triangle-up-left"></div>
        <div class="square-left"></div>
        <div class="triangle-up-right"></div>
        <div class="square-right"></div>
    </div>
    <div class="container">
        <h2 class="section_header fancy centered"> کادر اجرایی </h2>
        <div class="row">
            @foreach($except_professors as $excerpt)
                <div class="col-sm-6 col-md-3">
                    <div class="team_member">
                        <figure style="background-image:none">
                            @if(!$excerpt->Hasmedia('images'))
                                <img src="{{asset('img/no-img.gif')}}" width="150" height="150"  alt="1a">
                            @else
                                <img src="{{$excerpt->getFirstMediaUrl('images')}}" width="150" height="150" alt="1a">
                            @endif
                        </figure>
                        <h5>{{$excerpt->full_name}}</h5>
                        <small>{{$excerpt->role_name}}</small>
                        <hr>
                        <div class="team_social">
                            <a href="https://telegram.me/{{$excerpt->info->telegram}}"><i class="fa fa-telegram"></i></a>
                            <a href="https://www.instagram.com/{{$excerpt->info->instagram}}"><i class="fa fa-instagram"></i></a>
                            <a href="https://wa.me/{{$excerpt->info->whatsapp}}"><i class="fa fa-whatsapp"></i></a>
                        </div>
                        <p class="short_bio">{{$excerpt->about}} </p>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>

<section class="team_members">
    <div class="container triangles-of-section">
        <div class="triangle-up-left"></div>
        <div class="square-left"></div>
        <div class="triangle-up-right"></div>
        <div class="square-right"></div>
    </div>
    <div class="container">
        <h2 class="section_header fancy centered"> اساتید ما  </h2>
        <div class="row">
            @foreach($professors as $professor)
                <div class="col-sm-6 col-md-3">
                    <div class="team_member">
                        <figure style="background-image:none">
                            @if(!$professor->Hasmedia('images'))
                                <img src="{{asset('img/no-img.gif')}}" width="150" height="150"  alt="1a">
                            @else
                                <img src="{{$professor->getFirstMediaUrl('images')}}" width="150" height="150" alt="1a">
                            @endif
                        </figure>
                        <h5>{{$professor->full_name}}</h5>
                        <small>{{$professor->role_name}}</small>
                        <hr>
                        <div class="team_social">
                            <a href="https://telegram.me/{{$professor->info->telegram}}"><i class="fa fa-telegram"></i></a>
                            <a href="https://www.instagram.com/{{$professor->info->instagram}}"><i class="fa fa-instagram"></i></a>
                            <a href="https://wa.me/{{$professor->info->whatsapp}}"><i class="fa fa-whatsapp"></i></a>
                        </div>
                        <p class="short_bio">{{$professor->about}} </p>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>
