 <section id="slider_wrapper" class="slider_wrapper full_page_photo">
                <div id="main_flexslider" class="flexslider">
                    <ul class="slides">
                        @foreach($sliders as $slider)
                            @if($slider->Hasmedia('images'))
                                <li class="item" style="background-image: url({{$slider->getFirstMediaUrl('images')}})">
                                    @else
                                <li class="item" style="background-image: url({{asset('template/images/1.jpg')}})">
                                            @endif

                            <div class="container">
                                <div class="carousel-caption animated bounceInUp">
                                    <h1>{{$slider->title}} </h1>
                                    <p class="lead skincolored">{{$slider->excerpt}} </p>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
</section>
