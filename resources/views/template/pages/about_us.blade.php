@extends('template.app')

@section('content')

    <div class="full_page_photo" style="background-image: none;">
        <div class="hgroup">
            <div class="hgroup_title animated bounceInUp">
                <div class="container">
                    <h1 class="">درباره ما</h1>
                </div>
            </div>
            <div class="hgroup_subtitle animated bounceInUp skincolored">
                <div class="container">
                    <p>کارهای بزرگ با تیمی کوچک و صمیمی!</p>
                </div>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="container triangles-of-section">
            <div class="triangle-up-left"></div>
            <div class="square-left"></div>
            <div class="triangle-up-right"></div>
            <div class="square-right"></div>
        </div>
        <section class="horizontal_teaser">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4 horizontal_teaser_left">
                        <h3>{{$setting->name}}</h3>
<p>
    {{$setting->excerpt}}
</p>
                    </div>
                    <div class="col-sm-12 col-md-8 horizontal_teaser_right">

                            {!! $setting->google_map !!}
                    </div>
                </div>
            </div>
        </section>







@endsection
