
@extends('template.app')

@section('content')

    <div class="full_page_photo">
        <div id="map"></div>
        <div class="container">
            <div class="hgroup">
                <div class="hgroup_title animated bounceInUp ">
                    <div class="container">
                        <h1>ارتباط با ما</h1>
                    </div>
                </div>
                <div class="hgroup_subtitle animated bounceInUp skincolored">
                    <div class="container">
                        <p>از طریق فرم زیر میتوانید با ما در ارتباط باشید.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="container triangles-of-section">
            <div class="triangle-up-left"></div>
            <div class="square-left"></div>
            <div class="triangle-up-right"></div>
            <div class="square-right"></div>
        </div>
        <div class="container">
            <section>
                <div class="row">
                    <div class="office_address col-sm-6 col-md-4">
                        <div class="team_member">
                            @if(!$setting->Hasmedia('logo'))
                                <img src="{{asset('img/no-img.gif')}}"  alt="logo">
                            @else
                                <img src="{{$setting->getFirstMediaUrl('logo')}}"  alt="logo">
                            @endif

                            <h5>{{$setting->name}}</h5>

                            <abbr title="Phone">تلفن:</abbr> {{json_decode($setting->phone,true)[0]}}<br>
                            <abbr title="Phone">ایمیل:</abbr> <a href="mailto:#">support@tehrantebo.ir</a> </div>
                    </div>
                    <div class="contact_form col-sm-6 col-md-8">
                        <form name="contact_form" id="contact_form" method="post">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <label>نام</label>
                                    <input name="name" id="name" class="form-control" type="text" value="">
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <label>ایمیل</label>
                                    <input name="email" id="email" class="form-control" type="text" value="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <label>موضوع</label>
                                    <input name="subject" id="subject" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <label>پیام</label>
                                    <textarea name="message" id="message" rows="8" class="form-control"></textarea>
                                </div>
                                <div class="col-sm-12 col-md-12"><br/>
                                    <a id="submit_btn" class="btn btn-primary" name="submit">ارسال</a> <span id="notice" class="alert alert-warning alert-dismissable hidden" style="margin-left:20px;"></span> </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>

@endsection

