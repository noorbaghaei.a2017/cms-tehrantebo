@extends('template.app')

@section('content')

<div class="full_page_photo no_photo">
    <div class="hgroup">
        <div class="hgroup_title animated bounceInUp skincolored">
            <div class="container">
                <h1 class="">گالری عکس </h1>
            </div>
        </div>
        <div class="hgroup_subtitle animated bounceInUp ">
            <div class="container">
            </div>
        </div>
    </div>
</div>
<div class="main">
    <div class="container triangles-of-section">
        <div class="triangle-up-left"></div>
        <div class="square-left"></div>
        <div class="triangle-up-right"></div>
        <div class="square-right"></div>
    </div>
    <div class="container">
        <section>
            <ul class="portfolio_filters">
                <li><a href="#" data-filter="*">همه</a></li>
                @foreach($categories as $category)
                <li><a href="#" data-filter=".{{$category->id}}">{{$category->symbol}} </a></li>
                @endforeach
            </ul>
        </section>
        <section class="portfolio_masonry">
            <div class="row isotope_portfolio_container">
                @foreach($items as $item)
                <div class="{{$item->category}} col-sm-4 col-md-4">
                    <div class="portfolio_item">

                            @if(!$item->Hasmedia('images'))
                            <a href="{{asset('img/no-img.gif')}}" class="lightbox">
                                <img src="{{asset('img/no-img.gif')}}" alt="alt here" width="600" height="400">
                                    @else
                                        <a href="{{$item->getFirstMediaUrl('images')}}" class="lightbox">
                                            <img src="{{$item->getFirstMediaUrl('images')}}" alt="alt here" width="600" height="400">
                                            @endif


                            <div class="overlay">
                                <div class="desc">
                                    <h3>{{$item->title}}</h3>
                                    <span class="cross"></span> </div>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </section>
    </div>


@endsection
