<!-- Pricing Section -->
<section class="pricing-section spad">
    <div class="container">
        <div class="section-title text-center">
            <img src="{{asset('template/img/icons/logo-icon.png')}}" alt="">
            <h2>Pricing plans</h2>
            <p>Practice Yoga to perfect physical beauty, take care of your soul and enjoy life more fully!</p>
        </div>
        <div class="row">
            @foreach($plans as $plan)
                <div class="col-lg-3 col-sm-6">
                    <div class="pricing-item begginer">
                        <div class="pi-top">
                            <h4>{{$plan->title}}</h4>
                        </div>
                        <div class="pi-price">
                            <h3>${{$plan->price}}</h3>
                            <p>Per month</p>
                        </div>
                        <ul>
                            <li>Take Up To 7 Classes</li>
                            <li>Available To Anyone</li>
                            <li>Towels Included</li>
                            <li>Never Expires</li>
                        </ul>
                        <a href="#" class="site-btn sb-line-gradient">Get started</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!-- Pricing Section end -->
